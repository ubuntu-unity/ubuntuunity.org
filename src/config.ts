import type { Site, SocialObjects } from "./types";

export const SITE: Site = {
  website: "https://ubuntuunity.org/", // replace this with your deployed domain
  author: "Rudra Saraswat",
  desc: "A flavor of Ubuntu using the Unity7 desktop environment.",
  title: "Ubuntu Unity",
  lightAndDarkMode: true,
  postPerPage: 3,
};

export const LOCALE = ["en-EN"]; // set to [] to use the environment default

export const LOGO_IMAGE = {
  enable: false
};

export const SOCIALS: SocialObjects = [
  {
    name: "GitLab",
    href: "https://gitlab.com/ubuntu-unity",
    linkTitle: ` ${SITE.title} on GitLab`,
    active: true,
  },
  {
    name: "Twitter",
    href: "https://x.com/ubuntu_unity",
    linkTitle: `${SITE.title} on Twitter`,
    active: true,
  },
  {
    name: "Discord",
    href: "https://discord.gg/m5H5bmSYds",
    linkTitle: `${SITE.title} on Discord`,
    active: true,
  },
  {
    name: "Reddit",
    href: "https://reddit.com/r/UbuntuUnity",
    linkTitle: `${SITE.title} on Reddit`,
    active: true,
  },
  {
    name: "Telegram",
    href: "https://t.me/ubuntuunitydiscuss",
    linkTitle: `${SITE.title} on Telegram`,
    active: true,
  }
];
