---
pubDatetime: 2023-10-12T00:00:00
title: "Ubuntu Unity 23.10 released"
description: "Ubuntu Unity 23.10 has now been released."
image: "images/blog/ubuntu-unity-becomes-an-official-flavor.png"
author: "Rudra Saraswat"
draft: false
featured: true

tags:
  - flavor
  - release
  - stable
---

Ubuntu Unity 23.10 "Mantic Minotaur" has now been released! You can download it from https://ubuntuunity.org.

![](../../assets/images/ubuntu-unity-23.10.png)

Ubuntu Unity 23.10 continues to use Unity 7.7, which has undergone maintenance. Our primary focus for this release has been to gradually migrate away from Nux in UnityX,
which is progressing well. Nux is holding us back from offering Wayland support without Xwayland. We were also pre-occupied with adding support for CUPS 2.0 in Unity
(printing), which has now been pushed back by Ubuntu to 24.04.

![](../../assets/images/lomiri-23.10-incomplete.png)

A Lomiri variant of Ubuntu Unity is also in progress, and was supposed to be released today. Unfortunately, some app launcher-related bugs held us back,
and we're unable to make it available for download with the release announcement, but we hope to be able to do so within the next few days.

We'd like to give a shout out to Marius Gripsgard for getting Lomiri in the Debian repos (archives), and would like to thank UBports for all of the help with the Lomiri edition!

### Ubuntu Unity merch

Thanks to Maik Adamietz of the Ubuntu Unity team, we are glad to announce a collab between HelloTux and Ubuntu Unity.

From [HelloTux's website](https://www.hellotux.com/aboutus.php):

> We are not just another t-shirt shop with some Linux shirts - we are a shop with only Linux and free software shirts. It does really matter.

HelloTux is a family business and a global provider of shirts and merch for FOSS projects. We encourage you to check out their awesome merch, and do take a look at their Ubuntu Unity merch too!

https://www.hellotux.com/ubuntu-unity

### Additional information

You can also install Ubuntu Unity on an existing Ubuntu install by removing `gnome-shell` and other GNOME apps, and then installing the `ubuntu-unity-desktop` package
(all the Ubuntu Unity packages are in `universe` in the official Ubuntu archive).

A `do-release-upgrade -d` should get you up to speed if you're currently on 23.04 and would like to upgrade. (and `do-release-upgrade` eventually)

**Special thanks to Dmitry Shachnev (mitya57), our MOTU/uploader, who has been uploading all our packages to the universe pocket of the official Ubuntu Unity repositories, including many last-minute emergency uploads. This release would not have been possible without his help and support.**

**I would also like to thank Maik and Tobiyo, fellow members of the Ubuntu Unity team, who have actively and patiently been answering all your support-related queries, and moderating our Telegram, Matrix, IRC and Discord groups/servers.**

For support or queries, please join us on Telegram at [t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss), or on IRC (`#ubuntu-unity` on libera.chat).
