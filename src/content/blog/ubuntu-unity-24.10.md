---
pubDatetime: 2024-10-10T00:00:00
title: "Ubuntu Unity 24.10 released"
description: "Ubuntu Unity 24.10 has now been released."
author: "Rudra Saraswat"
draft: false
featured: true

tags:
  - flavor
  - release
  - stable
---

Ubuntu Unity 24.10 "Oracular Oriole" has now been released! You can download it from https://ubuntuunity.org.

![Ubuntu Unity 24.10 screenshot](../../assets/images/ubuntu-unity-24.10.png)

Ubuntu Unity 24.10 continues to use Unity 7.7, and has seen a move away from `unity-greeter` as a result of some bugs discovered only a short while before the release (after the archive freezes) to `lightdm-gtk-greeter`; we continue to use `lightdm`, of course (this bug also affected other flavours of Ubuntu that used `unity-greeter` atop `lightdm`).

As with Ubuntu Unity 24.04, the 24.10 installation images use Calamares. We would like to thank Simon Quigley and Aaron Rainbolt for integrating Calamares with our existing Ubuntu Unity live session, and last-minute fixes for bugs reported prior to the final release.

Alongside Ubuntu Unity 24.10 is Ubuntu Lomiri 24.10, significantly improved from the testing image released back in April for 24.04. Crashes are not nearly as frequent as they were on earlier images, and this image is in fact stable enough to daily drive, although we still wouldn't recommend installing it on your primary machine, unless you're comfortable with the idea of having to debug issues yourself; you're largely on your own from a support standpoint (you will not find support for this image on AskUbuntu or any other official Ubuntu support channels; the Ubuntu Unity Telegram group and UBports channels will be more appropriate for such questions).

We would like to thank the UBports team for developing and maintaining the Lomiri packages in Debian (and by extension Ubuntu).

Here's the link to the Lomiri ISO I built: https://ruds.io/cloud/s/kSGFnLJycGk9pM9

If the launcher/dock does not show up upon booting and you only see the Lomiri background, just press the Super/Windows key once for it (as well as dash to show up). The dock should automatically show up on subsequent logins.

![Ubuntu Lomiri 24.10](../../assets/images/lomiri-24.10.png)

![Ubuntu Lomiri 24.10 app launcher](../../assets/images/lomiri-24.10-launcher.png)

### Upgrades from 24.04

If you're on a laptop, make sure it is sufficiently charged or plugged in. A stable internet connection is also preferred.

You must run the following commands:

```bash
sudo sed -i 's/Prompt=lts/Prompt=normal/g' /etc/update-manager/release-upgrades
sudo apt install ubuntu-unity-desktop ubuntu-release-upgrader-core
sudo do-release-upgrade -d
```

### Ubuntu Unity merch

Thanks to Maik Adamietz of the Ubuntu Unity team, we are glad to announce a collaboration between HelloTux and Ubuntu Unity.

From [HelloTux's website](https://www.hellotux.com/aboutus.php):

> We are not just another t-shirt shop with some Linux shirts - we are a shop with only Linux and free software shirts. It does really matter.

HelloTux is a family business and a global provider of shirts and merch for FOSS projects. We encourage you to check out their awesome merch, and perhaps their Ubuntu Unity merch!

https://www.hellotux.com/ubuntu-unity

### Additional information

You can also install Ubuntu Unity on an existing Ubuntu installation by removing `gnome-shell` and other GNOME apps, then installing the `ubuntu-unity-desktop` package
(all the Ubuntu Unity packages are in `universe` in the official Ubuntu archive).

You may encounter missing close-maximize-minimize buttons in GNOME apps (since they use custom header bars/client-side-decorations). Removing the `libgtk3-nocsd0` package should help with that.

**Special thanks to Dmitry Shachnev (mitya57), our MOTU/uploader, who has been uploading all our packages to the universe pocket of the official Ubuntu Unity repositories, including many last-minute emergency uploads. This release would not have been possible without his help and support.**

**I would also like to thank Maik and Tobiyo, fellow members of the Ubuntu Unity team, who have actively and patiently been answering all your support-related queries, and moderating our Telegram, Matrix, IRC and Discord groups/servers.**

For support or queries, please join us on Telegram at [t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss), or on IRC (`#ubuntu-unity` on libera.chat).

