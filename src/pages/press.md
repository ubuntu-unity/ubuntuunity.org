---
layout: ../layouts/AboutLayout.astro
title: "Press"
---

1. **Forbes**

> If you’re yearning for the good ole’ Unity and Compiz days, I bring awesome tidings: someone’s shining a new spotlight on them, and the stage underneath is a brand new Linux distribution called Ubuntu Unity Remix 20.04.

[Read more »](https://www.forbes.com/sites/jasonevangelho/2020/05/12/a-surprising-new-remix-of-ubuntu-2004-revives-the-unity-desktop/?sh=1468558b275b)

2. **TechRepublic**

> If you’ve been waiting for the return of the Unity desktop, your wait is over. Ubuntu Unity is a fresh take on the once-defunct interface.

[Read more »](https://www.techrepublic.com/article/ubuntu-unity-brings-back-one-of-the-most-efficient-desktops-ever-created/)

3. **9to5Linux**

> I took Ubuntu Unity Remix 20.04 for a spin and it brought back good old memories for me. The spin looks great and works like a charm.

[Read more »](https://9to5linux.com/ubuntu-unity-21-10-released-to-keep-the-unity-desktop-alive-in-2021)

4. **Wikipedia**

> Ubuntu Unity is a Linux distribution based on Ubuntu, using the Unity interface in place of Ubuntu’s GNOME Shell. The first release was 20.04 LTS on 7 May 2020. Prior to the initial release it had the working names of Unubuntu and Ubuntu Unity Remix.

[Read more »](https://en.wikipedia.org/wiki/Ubuntu_Unity)

5. **Linux++, Destination Linux Network**

> Yes, this was the old Unity I used to know and love, but somehow it felt fresher. As I worked to regain muscle memory over the key-bindings (GNOME really can take over the way you control your system XD), the experience was smooth, graceful, and fun in a way that is unique to the Unity experience.

[Read more »](https://medium.com/linux-plus-plus/linux-may-17-2020-aff97aed3a05)
